# Third party imports
from sh import git


class RenderError(Exception):
    pass


status = git.status()

changes_in_quarto = any(
    src_file in status
    for src_file in ["sources", "ressources", "index.qmd", "_quarto.yml"]
)
if changes_in_quarto and "public" not in status:
    raise RenderError("You forget to render ExoDoc with: `quarto render`")
