# Pytest imports
import pytest

# Standard imports
import re
from pathlib import Path

# Third party imports
import requests
from bs4 import BeautifulSoup

PROJECT_PATH = Path(__file__).parent.parent
LINKS_HTTP_COMPILED_REGEX = re.compile(r"^http")


def get_all_html_links() -> set[str]:
    """
    Get all html links in the public folder.

    Returns:
        All html links in the public folder.
    """
    root = PROJECT_PATH / "public"
    files = list(root.glob("**/*.html"))

    res = set()

    for file in files:
        with open(file) as fp:
            soup = BeautifulSoup(fp, "html.parser")
        links = soup.find_all("a", {"href": LINKS_HTTP_COMPILED_REGEX})
        for link in links:
            res.add(link.get("href"))

    return res


ALL_HTML_LINKS = get_all_html_links()


@pytest.mark.parametrize("url", ALL_HTML_LINKS)
def test_links_do_not_return_404(url):
    result = requests.get(url, timeout=3)
    if result.status_code == 404:
        print(f"Fail on link: {url}.")
        assert False
