# ExoDoc

![banner](ressources/cover.png)

Repository of global documentation for exoplanet team.

You can find some useful documentation use in the exoplanet project of the
"Observatoire de Paris".

**Feel free to suggest corrections or new documentation via the issues system.**

## Documentations

You can read the documentation on a website: [👉 **Exodoc webpage** 👈](https://exoplanet.pages.obspm.fr/exodoc/)

## How to contribute to Exodoc

1. Clone the repository: `git clone git@gitlab.obspm.fr:exoplanet/exodoc.git`.
2. install dependancy:
  
  ```shell
  sudo apt install python3-sphinxcontrib.svg2pdfconverter
  quarto install tinytex
  ```

2. Create a well named branch: `git checkout -b FEATURE/my_super_new_addition_to_exodoc`.
3. Add/modify files with what you need and commit your change (multiple clean commits is better than a single monolitic commit). Remember to regenerate the documentation files (HTML version of the doc) via the command: `quarto render`.
4. Create a **Merge Request** on Gitlab describing your change with the maintainer (Ulysse Chausson) as the reviewer.
5. Do some changes if requested.
6. The Merge Request will be merged and your change integrated as soon as the maintainer accept it.

## Contacts

- Author: Ulysse CHOSSON (LESIA)
- Maintainer: Ulysse CHOSSON (LESIA)
- Email: <ulysse.chosson@obspm.fr>
- Contributors:
  - Pierre-Yves MARTIN (LESIA)

[**exoplanet.eu website**](http://exoplanet.eu/)
